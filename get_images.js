$(document).ready(function(){
	$.getJSON('//www.army.mil/medalofhonor/swenson/get_news.php', function(data)
	{
		var img_count = 0;
				
		for (var i = 0; i < data.length; i++)
		{
			$.each(data[i].images, function(key,image){
				var html_str = "";
				//console.log(image.title);
				if(image.title)
				{
					var img_url = image.page_url;
					var img_src = image.url_size3;
					var img_title = image.title;
					var img_description = image.alt;
					var img_date = image.date;
					var img_id = image.id;
					
					
					var max_title_length = 40;
					var max_description_length = 100;
					var date = img_date.split("-");
					var year = date[0];
					var month = get_month_name(parseInt(date[1]));
					var get_date = date[2].split(" ");
					var day = get_date[0];
					
					if(img_title.length > max_title_length)
					{
						img_title = img_title.substring(0, max_title_length);
						img_title += "...";
					}
					
					if(img_description.length > max_description_length)
					{
						img_description = img_description.substring(0, max_description_length);
						img_description += "...";
					}
					
					if(image.title)
					{
						html_str += "<li>";
						
						html_str += "<a href='https://www.army.mil/media/"+img_id+"'><img src='"+img_src+"' alt='"+img_description+"' /></a>";
						html_str += "<h3><a href='https://www.army.mil/media/"+img_id+"'>"+img_title+"</a></h3>";
						html_str += "<p>"+img_description+"</p>";
						html_str += "<a href='"+image.url+"' class='imageDownload' onclick=\"_gaq.push(['_trackEvent', 'downloads', 'jpg', '"+image.url+"', , true]);\">Download Hi-Res</a>";
						if(month != "NoMonth"){
							html_str += "<p class='imageDate'>"+month+" "+day+", "+year+"</p>";
						}
						html_str += "</li>";
						$("#imageGallery").append(html_str);
					}	
				}
				
			});
		}	
	});
});

function get_month_name(month)
{
	if(month == 0){return "NoMonth";}
	if(month == 1){return "January"; }
	if(month == 2){return "February"; }
	if(month == 3){return "March"; }
	if(month == 4){return "April"; }
	if(month == 5){return "May"; }
	if(month == 6){return "June"; }
	if(month == 7){return "July"; }
	if(month == 8){return "August"; }
	if(month == 9){return "September"; }
	if(month == 10){return "October"; }
	if(month == 11){return "November"; }
	if(month == 12){return "December"; }
	
}