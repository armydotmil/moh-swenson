$(document).ready(function(){
	$.getJSON('//www.army.mil/medalofhonor/swenson/get_news.php', function(data)
	{
		for (var i = 0; i < 100; i++)
		{
			if(i == 0)
			{
				var html_str = "";
				
				html_str += "<a href='"+data[i].page_url+"?from=swenson&page=news'><img alt='"+data[i].title+"' src='"+data[i].images[0].url_size2+"' class='right border'></a>";
				html_str += "<h3><a href='"+data[i].page_url+"?from=swenson&page=news'>"+data[i].title+"</a></h3>";
				html_str += "<p>"+data[i].description+"</p>";
				html_str += "<a href='"+data[i].page_url+"?from=swenson&page=news' class='mohButton'>Read More</a>";
				
				$("#topStory").append(html_str);
			} else if(i > 0 && i < 3)
			{
				var html_str = "";
				html_str += "<li>";
				html_str += "<a href='"+data[i].page_url+"?from=swenson&page=news'><img alt='"+data[i].title+"' src='"+data[i].images[0].url_size3+"' class='right border articleThumb'></a>";
				html_str += "<h3><a href='"+data[i].page_url+"?from=swenson&page=news'>"+data[i].title+"</a></h3>";
				html_str += "<p>"+data[i].description+"</p>";
				html_str += "<a href='"+data[i].page_url+"?from=swenson&page=news' class='mohButton'>Read More</a>";
				html_str += "</li>";
				
				$("#subStory").append(html_str);
				
			} else if(i > 2 && i < 7)
			{
				var date = data[i].date.split("-");
				var year = date[0];
				var month = get_month_name(date[1]);
				var get_date = date[2].split(" ");
				var day = get_date[0];
				var html_str = "";
				html_str += "<li>";
				html_str += "<a href='"+data[i].page_url+"?from=swenson&page=news'>"+data[i].title+"</a> - "+month+" "+day+", "+year;
				html_str += "</li>";
				$("#more_stories_right").append(html_str);
			} else if(i > 7){
				var html_str = "";
				html_str = "<li>";
				html_str += "<a href='"+data[i].page_url+"?from=swenson&page=news'>"+data[i].title+"</a> - "+month+" "+day+", "+year;
				html_str += "</li>";
				$("#moreStoriesList").append(html_str);
			}	
		}
	});
});

function get_month_name(month)
{
	if(month == 1){return "January"; }
	if(month == 2){return "February"; }
	if(month == 3){return "March"; }
	if(month == 4){return "April"; }
	if(month == 5){return "May"; }
	if(month == 6){return "June"; }
	if(month == 7){return "July"; }
	if(month == 8){return "August"; }
	if(month == 9){return "September"; }
	if(month == 10){return "October"; }
	if(month == 11){return "November"; }
	if(month == 12){return "December"; }
	
}